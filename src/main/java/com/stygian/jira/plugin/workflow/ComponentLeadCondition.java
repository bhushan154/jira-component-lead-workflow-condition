package com.stygian.jira.plugin.workflow;

import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.component.ComponentAccessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.workflow.condition.AbstractJiraCondition;
import com.opensymphony.module.propertyset.PropertySet;

import java.util.Collection;
import java.util.Map;

public class ComponentLeadCondition extends AbstractJiraCondition
{
    private static final Logger log = LoggerFactory.getLogger(ComponentLeadCondition.class);

    public boolean passesCondition(Map transientVars, Map args, PropertySet ps)
    {
        String username = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser().getName();
        Issue issue = getIssue(transientVars);
        ProjectComponentManager componentManager = ComponentAccessor.getProjectComponentManager();
        Collection<ProjectComponent> componentCollection = issue.getComponentObjects();
        for(ProjectComponent component:componentCollection){
            if(component.getLead().equals(username)){
                return true;
            }
        }
        return false;
    }
}
